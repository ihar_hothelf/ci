package com.hothelf.ihar.gradleci;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle pSavedInstanceState) {
        super.onCreate(pSavedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, getSoftwareVersion(), Toast.LENGTH_SHORT).show();
    }

    private CharSequence getSoftwareVersion() {
        final PackageInfo packageInfo;

        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            return packageInfo.versionName;
        } catch (final PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
